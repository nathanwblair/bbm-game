﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreController : MonoBehaviour {
	public int score;
	Text textComponent;
	
	//[ExposeProperty]
	public int Score {
		get {
			return score;
		}
		set	{
			score = value;
			textComponent.text = this.ToString();
		}
	}
	
	// Use this for initialization
	void Start () {
		textComponent = GetComponent<Text>();  
		Score = 0;  
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public override string ToString() {
		return string.Format("Score: {0}", score);
	}
}
