﻿using UnityEngine;
using System.Collections;

public class PlayerFeedbackController : PlayerComponent {
    float controllerVibration = 0f;

    private float ControllerVibration {
		get {
            return RumbleScale(controllerVibration);
        }
		set {
            controllerVibration = Mathf.Clamp(value, 0f, 1f);
        }
	}

    // Use this for initialization
    void Start () {
        InitPlayerComponent();
    }
	
	// Update is called once per frame
	void Update () {
        UpdateVibration();
    }
	
	public override void OnTug() {
        IncrementControllerVibration();
    }
	
	void UpdateVibration() {
		
	}
	
	void IncrementControllerVibration() {
        ControllerVibration += 0.1f;
    }
	
	void ClearControllerVibration() {
        ControllerVibration = 0;
    }
	
	float RumbleScale(float value) {
        return (Mathf.Log(value) + 2.2f) / 2.2f;
    }
}
