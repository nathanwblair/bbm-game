﻿using UnityEngine;
using System.Collections;

public abstract class PlayerComponent : MonoBehaviour {
    protected PlayerController controller;
	
	protected void InitPlayerComponent() {
        controller = GetComponent<PlayerController>();
    }

    public virtual void OnDeath(PlayerController killer) {
		// Do nothing
	}
	
	public virtual void OnWin(PlayerController playerTheMadeTheWinningStroke) {
		// Do nothing
	}
	
	public virtual void OnLose(PlayerController playerTheMadeTheWinningStroke) {
		// Do nothing
	}
	
	public virtual void OnKill(PlayerController victim) {
		// Do nothing
	}
	
	public virtual void OnTug() {
		// Do nothing
	}
}
