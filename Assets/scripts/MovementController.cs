﻿using UnityEngine;
using System.Collections;

public class MovementController : MonoBehaviour
{
    private Rigidbody m_rb;
    public float m_speed;
    private float m_horizontal;
    public float m_timer;
    public int m_buttonCount;
    
    public PlayerController m_controller;
    // Death code starts on line 71

    void Awake() {}
	// Use this for initialization
	void Start ()
    {
//        m_controller = GetComponent<PlayerController>();
        m_rb = GetComponent<Rigidbody>();
        m_speed = 5.0f;
        m_timer = 0.5f;
        m_buttonCount = 0;
        StartCoroutine(SpeedRefresh());
    }
	
	// Update is called once per frame
	void Update ()
    {
        // resetting speed
        m_timer -= Time.deltaTime;

     
        

        m_horizontal = Input.GetAxis("Horizontal");

        // movement
        if(Input.GetKeyDown(KeyCode.LeftArrow))
           {
            
             m_horizontal = -1;
            m_buttonCount += 1;

            if(m_buttonCount > 3)
            {
                m_speed += 10.0f;
            }

           }
        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
           
            m_horizontal = 1;
            m_buttonCount += 1;

            if (m_buttonCount > 3)
            {
                m_speed += 10.0f;
            }
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
                m_speed = m_speed * 3;
               
        }
       
	}


    void TriggerDeath()
    {
        m_controller.TriggerDeath();
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Trap")
        {
            // death
            TriggerDeath();
        }
    }


    void FixedUpdate()
    {
        m_rb.velocity = new Vector3(m_horizontal * m_speed, 0.0f, 0.0f);
    }

    IEnumerator SpeedRefresh()
    {
        m_speed = 0.0f;
        yield return new WaitForSeconds(0.4f);
        StartCoroutine(SpeedRefresh());
    }
}
