﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TeamController : MonoBehaviour {
	public PlayerCollectionController players;
    public Tug currentTug;
    public bool isWinning = false;
    public TeamController other;
	
    private GameController game;
    public ScoreController score;

    public Direction side;


    // Use this for initialization
    void Awake () {
        players = GetComponentInChildren<PlayerCollectionController>();
	}
	
	// Update is called once per frame
	void Update () {
        if (players.Size == 0) {
            game.TriggerWin();
        }
	}
	
	public bool TryTug(Tug tug) {
        return game.TryTug(tug);
    }
	
	public void Init(TeamController _otherTeam, Direction _side, GameController _game) {
        other = _otherTeam;
        side = _side;
        game = _game;
        players.Init(this);
    }

    public void TriggerDeath(PlayerController victim) {
        game.TriggerDeath(victim);
    }
	
	public void OnDeath(PlayerController killer, PlayerController victim) {
        victim.OnDeath(killer);
    }
	
	public void OnKill(PlayerController killer, PlayerController victim) {
        killer.OnKill(victim);
    }
	
	public void OnWin(PlayerController winner) {
        score.Score++;
		
		if (score.Score >= game.scoreToWin) {
            game.TriggerGameOver(this);
        }

        foreach(var player in players.Get()) {
            player.OnWin(winner);
        }
    }
	
	public void OnLose(PlayerController winner) {
		foreach(var player in players.Get()) {
            player.OnLose(winner);
        }
    }

    public void WinWithTug(Tug tug)
    {
        isWinning = true;
        
        if (currentTug == null) {
            currentTug = tug;
        }
        else {
            currentTug.ChangeIfNeeded(tug, tug.tugger.isCharging);
        }
        
		if (!tug.tugger.isCharging) {
            game.UseWinningTug();
        }
    }
	
	public string GetName() {
        return "Team" + ((int)side + 1).ToString();
    }
	
	public void Charge(Tug tug) {
        game.Charge(tug);
        tug.tugger.isCharging = false;
    }
}
