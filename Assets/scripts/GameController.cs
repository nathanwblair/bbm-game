﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Tug {
	public enum Type {
		Rock,
		Paper,
		Scissors
	}

    public int strength = 1;

    public PlayerController tugger;

    public Type type;
    static Dictionary<Type, Type> takes = new Dictionary<Type, Type>();

    public Tug(Type _type, PlayerController _tugger) {
		if (takes.Count == 0) {
			PopulateTakesMap();
		}
        
        tugger = _tugger;
        type = _type;
	}
	
	public bool Beats(Tug other) {
		if (takes[type] == other.type) {
            return true;
        }
		else {
            return false;
        }
	}
	
	public bool Is(Tug other) {
		if (other.type == this.type) {
            return true;
        }
		else {
            return false;
        }
	}
	
	public void ChangeIfNeeded(Tug newTug, bool addTugAsCharge=false) {        
		if (Is(newTug) && (addTugAsCharge || tugger.isCharging)) {
			strength += newTug.strength;
        }
		else {
            type = newTug.type;
            strength = newTug.strength;
        }
	}
	
	public void IncrementStrength() {
        strength++;
    }
	
	static void PopulateTakesMap() {
        takes.Add(Type.Rock, Type.Scissors);
        takes.Add(Type.Paper, Type.Rock);
        takes.Add(Type.Scissors, Type.Paper);
    }
}


public class GameController : MonoBehaviour {   
	
    public Tug WinningTug {
		get {
            var teamOneTug = leftTeam.currentTug;
            var teamTwoTug = rightTeam.currentTug;
            
            var isOneNull = teamOneTug == null;
            var isTwoNull = teamTwoTug == null;
            
            if (isOneNull) {
                if (isTwoNull) {
                    return null;
                }
                else {
                    return teamTwoTug;
                }
            }
            else if (isTwoNull) {
                if (isOneNull) {
                    return null;
                }
                else {
                    return teamOneTug;
                }
            }
            else if (teamOneTug.Beats(rightTeam.currentTug)) {
                return teamOneTug;
            }
			else if (teamTwoTug.Beats(teamOneTug)) {
                return teamTwoTug;
            }
            else {
                return null;
            }            
        }
		
		set {
            bool isValueValid = true;
            
            if (WinningTug != null)  {
                if (WinningTug.tugger.team.Equals(value.tugger.team)) {
                    isValueValid = WinningTug.Is(value);
			    }
                else if (WinningTug.Beats(value)) {
                    isValueValid = false;
                }
            }
            
            if (isValueValid) {
                value.tugger.team.WinWithTug(value);
                WinningTug.tugger.team.other.isWinning = false;
            }
		}
	}

    public void Charge(Tug tug)
    {
        if (WinningTug.tugger.team.Equals(tug.tugger.team)) {
            UseWinningTug();
        }
    }

    public Tug LosingTug;
    public int scoreToWin;

    private TeamController leftTeam;
    private TeamController rightTeam;

    public float speed = 10f;

    private Rigidbody rigidbody;

    private RopeController rope;
    public GameManager manager;

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rope = GetComponentInChildren<RopeController>();
        
        leftTeam = transform.Find("TeamOne").GetComponent<TeamController>();
        rightTeam = transform.Find("TeamTwo").GetComponent<TeamController>();
        
        leftTeam.Init(rightTeam, Direction.Left, this);
        rightTeam.Init(leftTeam, Direction.Right, this);

       // rope.AddSegment(Direction.Left);
       // rope.AddSegment(Direction.Right);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TriggerDeath(PlayerController victim)
    {
        var killer = WinningTug.tugger;

        killer.team.OnKill(killer, victim);
        victim.team.OnDeath(killer, victim);
    }

    public void TriggerWin()
    {
        var winner = WinningTug.tugger;
        var winningTeam = winner.team;
        var losingTeam = winningTeam.other;

        winningTeam.OnWin(winner);
        losingTeam.OnLose(winner);
        
        manager.Rematch();
    }
	
	public void TriggerGameOver(TeamController winningTeam) {
        manager.TriggerGameOver(winningTeam);
    }
	
	public void OnGameOver(TeamController winningTeam) {
        
	}

    public bool TryTug(Tug tug)
    {
        if (WinningTug == null) {
            WinningTug = tug;

            return true;            
        }
		else if (WinningTug.tugger.team.Equals(tug.tugger.team)) {
            if (WinningTug.Is(tug)) {
                WinningTug = tug;
                return true;
            }
            return false;
        }
        else if (tug.Beats(WinningTug)) {
            WinningTug = tug;

            return true;
        }

        return false;
    }
	
    public void UseWinningTug()
    {
        int direction = -1;

        if (WinningTug.tugger.team.Equals(rightTeam)) {
            direction = 1;
        }
		
        Move(WinningTug.strength * direction);
    }

    private void Move(int strength)
    {
        float force = (float)strength * speed;

        rigidbody.AddForce(new Vector3(force, 0, 0), ForceMode.Impulse);
    }
    
    
    void OnTriggerEnter(Collider other) {
        if (other.tag == "Trap") {
            TriggerDeath(WinningTug.tugger.team.other.players.GetFirst());
        }
    }
}
