﻿using UnityEngine;
using System.Collections;

public class PlayerInputController : PlayerComponent {

	// Use this for initialization
	void Awake () {
        InitPlayerComponent();
    }
	
	// Update is called once per frame
	void Update () {
        UpdateChargeState();
        CheckForTug();
    }
	
	void UpdateChargeState() {
        controller.isCharging = IsCharging();
		
		if (GetButtonUp("Charge") && controller.tug != null) {
            controller.team.Charge(controller.tug);
        }
    }
	
	void CheckForTug() {
		if (GetButtonDown("Rock")) {
            controller.Tug(Tug.Type.Rock);
        }
		else if (GetButtonDown("Paper")) {
            controller.Tug(Tug.Type.Paper);			
		}
		else if (GetButtonDown("Scissors")) {
            controller.Tug(Tug.Type.Scissors);
		}		
    }
	
	bool IsCharging() {
        return GetButton("Charge");
    }
	
	bool GetButton(string type) {
        return Input.GetButton(GetButtonName(type));
	}
	
	bool GetButtonDown(string type) {
        return Input.GetButtonDown(GetButtonName(type));
	}
	
	bool GetButtonUp(string type) {
        return Input.GetButtonUp(GetButtonName(type));
	}
	
	string GetButtonName(string type) {
        return controller.team.GetName() +  type + controller.number.ToString();
	}
}
