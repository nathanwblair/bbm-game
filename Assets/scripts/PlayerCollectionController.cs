﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCollectionController : MonoBehaviour {
	public GameObject prefab;
    public int editor_size;

    private GameObject playerOneSocket;
    private TeamController team;
    
    private List<PlayerController> garbage = new List<PlayerController>();

    public int Size {
		get {
			return transform.childCount - 1;
		}
		set {
			var difference = value - Size;
            
            ChangeSize(difference);
        }
	}

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        Size = (int)editor_size;
        
        UpdateGarbage();
    }
    
    void UpdateGarbage() {
        var newGarbage = new List<PlayerController>();
        foreach(var item in garbage) {
            if (item.isDead)
                GameObject.DestroyImmediate(item.gameObject);
            else
                newGarbage.Add(item);
        }
        
        garbage.Clear();
        garbage = newGarbage;        
    }
	
	public List<PlayerController> Get() {
		return new List<PlayerController>(
			GetComponentsInChildren<PlayerController>());
	}
	
	public PlayerController Get(int playerNumber) {        
		return GetGO(playerNumber)
			.GetComponent<PlayerController>();
	}
	
	public void Init(TeamController _team) {
        playerOneSocket = transform.Find("Sockets").Find("First").gameObject;
        team = _team;
        Size = (int)editor_size;
    }
	
	public GameObject GetGO(int playerNumber) {
		return transform.Find("Player" + playerNumber.ToString()).gameObject;
	}
    
    public PlayerController GetFirst() {
        return transform.GetChild(1).GetComponent<PlayerController>();
    }
	
	public void Pop() {
		Remove(Size - 1);
	}
	
	public void Pop(int count) {
		for (int i = 0; i < count; i++) {
			Pop();
		}
	}
	
	public PlayerController Peek() {
		return Get(Size);
	}
	
	public Transform PeekTransform() {
		if (Size == 0) {
            return playerOneSocket.transform;
        }
		else {
            return Peek().transform;
        }
	}
	
	public void Add(int count) {
		for (int i = 0; i < count; i++) {
			Add();
        }		
	}
	
	public void Add() {
        var newPlayer = GameObject.Instantiate(prefab);
		
		var newController = newPlayer.GetComponent<PlayerController>();
		newController.Init(PeekTransform().position, Size + 1, transform, team);
	}
	
	public void Remove(PlayerController player) {
		Remove(player.number);
	}
	
	public void Remove(int playerNumber) {
        garbage.Add(Get(playerNumber));
	}
	
	public void Remove(GameObject player) {
        GameObject.DestroyImmediate(player);
	}
	
	// If count is negative, count number of players will be removed;
	public void ChangeSize(int count) {
        if (count < 0) {
			Pop(Mathf.Abs(count));
		}
		else if (count > 0) {
			Add(count);
		}
        
        editor_size = Size;
	}
}
