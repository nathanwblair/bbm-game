﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerController : MonoBehaviour {
	List<PlayerComponent> components;
	public TeamController team;
    public Tug tug;

    public bool isCharging = false;

    OnPlayerEvent onWin;
	OnPlayerEvent onLose;
	OnPlayerEvent onDeath;
	OnPlayerEvent onKill;
	OnTugEvent onTug;
	
	public int number;
	public bool wasCharging = false;
	public Transform next;
	
	private delegate void OnPlayerEvent(PlayerController subject);
	
	private delegate void OnTugEvent();
    
    public bool isDead = false;
    
    public float dyingTime = 3f;
    private float dyingTimer = 0f;

    // Use this for initialization
    void Start () {
		components = new List<PlayerComponent>(GetComponents<PlayerComponent>());
		foreach(PlayerComponent component in components) {
			onWin += component.OnWin;
			onLose += component.OnLose;
			onDeath += component.OnDeath;
			onKill += component.OnKill;
			onTug += component.OnTug;
		}
		
		next = transform.FindChild("Sockets").FindChild("Next");
	}

    public void Init(Vector3 position, int playerNumber, Transform parent, TeamController _team)
    {
		number = playerNumber;
		transform.position = position;

        team = _team;

        gameObject.name = "Player" + playerNumber.ToString();
        transform.SetParent(parent);
        
        if (team.side == Direction.Right) {
            this.transform.rotation = Quaternion.Euler(0f, 180, 0f);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (dyingTimer > 0) {
            dyingTimer -= Time.deltaTime;
        }
        if (dyingTimer < 0) {
            isDead = true;
        }
	}
    
	public void Tug(Tug.Type type)
    {
        var attemptedTug = new Tug(type, this);
        var success = team.TryTug(attemptedTug);
		
		if (success) {
            tug = attemptedTug;
        }
    }
	
	public virtual void OnDeath(PlayerController killer) {
		onDeath(killer);
        team.players.Remove(this);
        
        dyingTimer = dyingTime;
	}
	
	public virtual void OnWin(PlayerController playerTheMadeTheWinningStroke) {
		onWin(playerTheMadeTheWinningStroke);
		// Do nothing
	}
	
	public virtual void OnLose(PlayerController playerTheMadeTheWinningStroke) {
		onLose(playerTheMadeTheWinningStroke);
		// Do nothing
	}
	
	public virtual void OnTug() {
        onTug();
    }

    public virtual void OnKill(PlayerController victim) {
		onKill(victim);
		// Do nothing
	}

    public void TriggerDeath() {
		team.TriggerDeath(this);
    }
}
