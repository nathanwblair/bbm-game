﻿using UnityEngine;
using System.Collections;

public enum Direction {
	Left,
	Right
}

public class RopeController : MonoBehaviour {
    public GameObject segmentPrefab;

    private GameObject leftSegment;
    private GameObject rightSegment;


    // Use this for initialization
    void Awake () {
        leftSegment = transform.Find("LeftSegment").gameObject;
        rightSegment = transform.Find("RightSegment").gameObject;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void AddSegment(Direction side) {
        GameObject last;

        if (side == Direction.Left) {
            last = Last(leftSegment.transform).gameObject;
        }
		else {
            last = Last(rightSegment.transform).gameObject;
        }

        var newSegment = GameObject.Instantiate(segmentPrefab);
        newSegment.transform.SetParent(last.transform);
    }
	
	private Transform Last(Transform segment) {
		if (segment.GetChild(1).childCount == 0) {
            return segment;
        }
		else {
        	return Last(segment.GetChild(1).GetChild(0));
		}
    }
}
